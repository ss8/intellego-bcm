package com.ss8.intellego.bcm.replication.elastic.config;

import com.ss8.intellego.bcm.replication.elastic.exception.ElasticReplicationException;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

@Configuration
public class ElasticClientConfig extends AbstractElasticsearchConfiguration {

    @Value("${elastic.host.ports:localhost:9200}")
    private String hostports;



    @Override
    @Bean
    public RestHighLevelClient elasticsearchClient() {

        if(null == hostports || hostports.length()==0) {
            throw new ElasticReplicationException("Invalid value for property elastic.host.ports");
        }

        final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
            .connectedTo(hostports.split(","))
            .build();

        return RestClients.create(clientConfiguration).rest();
    }
}