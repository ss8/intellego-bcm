package com.ss8.intellego.bcm.replication.elastic.exception;

public class ElasticReplicationException extends RuntimeException {

    public ElasticReplicationException(String message) {
        super(message);
    }
}
