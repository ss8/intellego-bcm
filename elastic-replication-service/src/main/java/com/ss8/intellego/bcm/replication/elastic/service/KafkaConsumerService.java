package com.ss8.intellego.bcm.replication.elastic.service;

import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KafkaConsumerService {

    private final Logger logger = LoggerFactory.getLogger(KafkaConsumerService.class);

    @Value("${spring.kafka.consumer.topic.name:ELASTIC_INDEX_REQUEST}")
    private String consumerTopic;

    @Autowired
    private RestHighLevelClient elasticRestClient;

    @KafkaListener(topics = "${spring.kafka.consumer.topic.name:ELASTIC_INDEX_REQUEST}",
            groupId = "${spring.kafka.consumer.group-id}")
    public void consume(@Payload List<IndexRequest> indexRequests) {

        try {
            BulkRequest bulkRequest = new BulkRequest();
            for (IndexRequest indexRequest : indexRequests) {
                bulkRequest.add(indexRequest);
            }
            elasticRestClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        } catch (Exception e) {
            logger.error("Error while inserting bulk request " + consumerTopic, e);
        }
    }
}
